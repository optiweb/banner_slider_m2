#Magento 2 - Banner slider
This extension will connect your Magento 2 store with Mailerlite mailing service.

___

##Features:
- create slider with maximum 5 slides
- add image, link and HTML content to each slide

##Compatibility:
Tested in Magento 2.1.8. It should work with all Magento 2 installations

##Known issues:
- there are currently no known issues

##Installation:
1. install module:
    1. `composer require optiweb/module-bannerslider` OR
    1. download a zip and upload contents to `app/code/Optiweb/BannerSlider` folder
2. enable module: `php bin/magento module:enable Optiweb_BannerSlider`
2. run `php bin/magento setup:upgrade` from the root directory of your Magento installation
3. go to `Content > Widget` and create new widget
4. select type "Banner Slider"

##Licence:
MIT. (see LICENCE.txt)