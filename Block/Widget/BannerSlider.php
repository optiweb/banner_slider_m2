<?php
/**
 * @author Urban Kovač
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 * @package Optiweb_BannerSlider
 */

namespace Optiweb\BannerSlider\Block\Widget;

class BannerSlider extends AbstractBannerSlider
{
    protected $_template = "widget/banner_slider.phtml";

    public function getSlides()
    {
        $slides = array();
        if (!empty($this->hasData('img_1')) && !empty($this->hasData('url_1'))) {
            $slides[] = [
                'img'       => $this->getData('img_1'),
                'url'       => $this->getData('url_1'),
                'content'   => $this->getData('content_1'),
            ];
        }
        if (!empty($this->hasData('img_2')) && !empty($this->hasData('url_2'))) {
            $slides[] = [
                'img'       => $this->getData('img_2'),
                'url'       => $this->getData('url_2'),
                'content'   => $this->getData('content_2'),
            ];
        }
        if (!empty($this->hasData('img_3')) && !empty($this->hasData('url_3'))) {
            $slides[] = [
                'img'       => $this->getData('img_3'),
                'url'       => $this->getData('url_3'),
                'content'   => $this->getData('content_3'),
            ];
        }
        if (!empty($this->hasData('img_4')) && !empty($this->hasData('url_4'))) {
            $slides[] = [
                'img'       => $this->getData('img_4'),
                'url'       => $this->getData('url_4'),
                'content'   => $this->getData('content_4'),
            ];
        }
        if (!empty($this->hasData('img_5')) && !empty($this->hasData('url_5'))) {
            $slides[] = [
                'img'       => $this->getData('img_5'),
                'url'       => $this->getData('url_5'),
                'content'   => $this->getData('content_5'),
            ];
        }
        return $slides;
    }

}
