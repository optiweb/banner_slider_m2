<?php
/**
 * @author Urban Kovač
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 * @package Optiweb_BannerSlider
 */

namespace Optiweb\BannerSlider\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

abstract class AbstractBannerSlider extends Template implements BlockInterface
{

    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}