<?php
/**
 * @author Atwix
 * @package Optiweb_BannerSlider
 */

namespace Optiweb\BannerSlider\Block\Adminhtml\Widget;

use Magento\Framework\Data\Form\Element\AbstractElement as Element;
use Magento\Backend\Block\Template\Context as TemplateContext;
use Magento\Framework\Data\Form\Element\Factory as FormElementFactory;
use Magento\Backend\Block\Template;
use Magento\Theme\Helper\Storage;

class ImageChooser extends Template
{
    const URL_BASE = 'cms/wysiwyg_images/index';
    
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;
    
    /**
     * @param TemplateContext $context
     * @param FormElementFactory $elementFactory
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        FormElementFactory $elementFactory,
        $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }
    
    /**
     * Prepare chooser element HTML
     *
     * @param Element $element
     * @return Element
     * @throws \Magento\Framework\Exception\LocalizedException
 
     */
    public function prepareElementHtml(Element $element)
    {
        $config = $this->_getData('config');
        $config['add_directives'] = true;
        $sourceUrl = $this->getUrl(self::URL_BASE, [
            'target_element_id' => $element->getId(),
            Storage::PARAM_CONTENT_TYPE => \Magento\Theme\Model\Wysiwyg\Storage::TYPE_IMAGE
        ]);
        
        /** @var \Magento\Backend\Block\Widget\Button $chooser */
        $chooser = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                        ->setType('button')
                        ->setClass('btn-chooser')
                        ->setLabel($config['button']['open'])
                        ->setOnClick("MediabrowserUtility.openDialog('$sourceUrl',null, null,'".$this->escapeQuote(__('Choose Image...'),true)."',(opt = new Object(), opt.closed = false, opt))")
                        ->setDisabled($element->getReadonly());
        
        /** @var \Magento\Framework\Data\Form\Element\Text $input */
        $input = $this->elementFactory->create("text", ['data' => $element->getData()]);
        $input->setId($element->getId());
        $input->setForm($element->getForm());
        $input->setClass("widget-option input-text admin__control-file-wrapper");
        if ($element->getRequired()) {
            $input->addClass('required-entry');
        }
        
        $element->setData('after_element_html', $input->getElementHtml() . $chooser->toHtml());
        
        return $element;
    }
}