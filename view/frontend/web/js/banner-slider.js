
require(['jquery', 'domReady!'],function($){


    $('#banner_slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: false,
        speed: 600,
        autoplay: true,
        autoplaySpeed: 5000
    });

});